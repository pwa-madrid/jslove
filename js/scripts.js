const instalarButton = document.querySelector('.instalar');

function muestraBoton() {
  instalarButton.removeAttribute('hidden');
}

function ocultaBoton() {
  instalarButton.setAttribute('hidden', true);
}
