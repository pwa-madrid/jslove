# JSLove
## Enunciado
1. En esta carpeta tienes una web funcionando. El ejercicio consiste en convertirla en una PWA.
2. Crea un archivo package.json e instala un servidor como dependencia local de desarrollo. Levanta el servidor y comprueba que la web funciona correctamente.
3. Crea el manifiesto y línkalo desde index.html. Asegúrate de que la aplicación sólo funciona en vertical.
4. Genera, a partir del icono icono.png, todos los iconos para Android y iOS. Actualiza el manifiesto con los iconos de Android, y añade al index.html todos los links necesarios.
5. Comprueba con las DevTools de Chrome que el manifiesto está correcto.
6. Vamos a realizar una primera comprobación de los iconos:
    - Revisa la pestaña Network de las DevTools para ver si hay alguna carga incorrecta. Si la hay, revisa las rutas a los iconos (tanto en el manifiesto como en el index.html).
    - Comprueba que en la pestaña del Chrome aparece el favicon.
    - En el emulador de Android, abre la web en Chrome y dale a añadir a la pantalla de inicio. 
7. Lanza una auditoría con Lighthouse. Sólo deberían verse errores relacionados con el Service Worker.
8. Crea un Service Worker en la raíz del proyecto.
9. En js/scripts.js realiza el registro del Service Worker. Recuerda hacerlo sólo si el navegador soporta Service Workers, y cuando el usuario ya esté viendo toda la web.
10. En el Service Worker, implementa un listener para el evento fetch (que no haga nada).
11. Comprueba si la PWA ya es instalable. Instálala tanto en desktop como en un móvil real (ponla en internet antes).
12. En el Service Worker vamos a imprimir por consola información sobre las peticiones a la red. Recuerda configurar la consola para que también te imprima la petición inicial al documento HTML.
13. Para cada petición, el SW debe imprimir:
    - La URL de destino.
    - El tipo de recurso (hazlo sólo para imágenes, estilos y scripts, en el resto de casos que imprima "Otros")
    - Si es un recurso de nuestra app o es externo.
14. Por último, vamos a mostrar un botón de instalación. Haz que aparezca cuando la PWA sea instalable, y haz también que no aparezca automáticamente el cuadro de diálogo nativo para instalar la aplicación.
15. Cuando el usuario haga clic en el botón, debe aparecer el cuadro de diálogo para instalar la aplicación y desaparecer el botón. Si decide instalarla, lanza un alert ('lo siento!) dando las gracias.
